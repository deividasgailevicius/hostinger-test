<?php
if($_SERVER["HTTPS"] != "on") {
    header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<meta name="theme-color" content="#666">
	<meta name="description" content="">
	<meta name="author" content="Deividas Gailevičius">
	<meta name="robots" content="noindex">

	<link href="/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">
	
    <title>Darkas.lt</title>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<style type="text/css">
		html,
		body {
		  background-color: #eee;
		}
		.th {
			cursor: pointer;
		}
		.tr {
			background-color: #666;
			color: #fff;
		}

		.row div {
			padding-bottom: 5px;
			padding-top: 5px;
		}

		.bg-1 {
			background-color: #fff;
		}
		.bg-2 {
			background-color: #eee;
		}
		.container {
			max-width: 600px;
			width: 100%;
		}
		.selected {
			position: fixed;
			top: 20px;
			right: 50px;
			min-width: 200px;
		}
		#pages span, .unlimited {
			font-weight: bold;
			cursor: pointer;
		}
		#table .row div {
			cursor: pointer;
		}
		#table .row div:hover {
			background-color: #cec2e6
		}
		#table .row:hover {
			background-color: #cee2e6
		}
		.active {
			background-color: #cee2e6
		}
	</style>

</head>
<body>

	<div class="selected">
		<strong>Selected:</strong> <span class="content"></span>
	</div>

	<div class="container">
		<div class="spacer"></div>

		<div class="row text-center font-weight-bold tr"><div class="col-2 th" data-sortby="id" data-sort="asc">ID</div><div class="col-4 th" data-sortby="date" data-sort="asc">Date</div><div class="col-2 th" data-sortby="sys" data-sort="asc">SYS</div><div class="col-2 th" data-sortby="dia" data-sort="asc">DIA</div><div class="col-2 th" data-sortby="pulse" data-sort="asc">PULSE</div></div>
		<div class="text-center" id="table"></div>
		<div class="row">
			<div class="col-8"><div id="pages"></div></div>
			<div class="col-4 text-right"><span class="unlimited">Show All entries</span></div>
		</div>
		

		<script type="text/javascript">
		var txt = "";
		var sortby = "date"
		var sort = "desc"
		var arrItems = []
		var odj_sorted = []
		var limit = 15
		var currentPage = 1


		var jqxhr = $.get( "https://darkas.lt/spaudimas.php?api=full", callbackFuncWithData)

		function callbackFuncWithData(data){
		    $.each(data, function (index, value) {
		    	arrItems.push(value);
		    });
		    formatTable(arrItems, sortby, sort, currentPage)

			$(".th").click(function(){
				sortby = $(this).data("sortby")
				sort = $(this).data("sort")
				if (sort === "asc") {$(this).data("sort", "desc")}else{$(this).data("sort", "asc")}
				formatTable(arrItems, sortby, sort)
			})

			$(".unlimited").click(function(){
				formatTable(arrItems, sortby, sort, 1, 5000)
				$(this).empty()
				$("#pages").empty()
			})

		}


		function formatTable(arrItems, sortby, sort, currentPage = 1, limit = 15){

			odj_sorted = sortByKey(arrItems, sortby, sort);
			var i 
			var k = 0
			$.each(odj_sorted, function( key, myobj ){
		  		if (k >= ((parseInt(currentPage)-1)*limit) && k < (limit * parseInt(currentPage))) {
					i = ((i !== 1) ? 1 : 2)
			  		txt += '<div class="row bg-' + i + '"><div class="col-2">' +  myobj.id + '</div><div class="col-4">' +  myobj.date + '</div><div class="col-2">' + myobj.sys + '</div><div class="col-2">'+ myobj.dia + '</div><div class="col-2">' +  myobj.pulse + '</div></div>';
		  		}
		  		k++
		  	})
		  	$("#table").html(txt);
		  	txt = ''
		  	var pages = Math.ceil(odj_sorted.length/limit)
		  	var pagination = ""
		  	

		  	if (parseInt(currentPage) >= (pages - 5) && parseInt(currentPage) <= pages) {
		  		pagination = " <span>1</span> ... "
		  		for (var i = (parseInt(currentPage) - 4); i <= pages; i++) {
		  			if (i == parseInt(currentPage)) {
		  				pagination += ' ' + i + ' '
		  			}else{
		  				pagination += ' <span>' + i + '</span> '
		  			}
		  		}
		  	}

		  	if (parseInt(currentPage) > 5 && parseInt(currentPage) < (pages - 5)) {
		  		pagination = " <span>1</span> ... "
		  		for (var i = (parseInt(currentPage) - 4); i <= (parseInt(currentPage) + 4); i++) {
		  			if (i == parseInt(currentPage)) {
		  				pagination += ' ' + i + ' '
		  			}else{
		  				pagination += ' <span>' + i + '</span> '
		  			}
		  		}
		  		pagination += ' ... <span>' + pages + '</span> '
		  	}

		  	if (parseInt(currentPage) <= 5) {
		  		for (var i = 1; i < (parseInt(currentPage) + 4); i++) {
		  			if (i == parseInt(currentPage)) {
		  				pagination += ' ' + i + ' '
		  			}else{
		  				pagination += ' <span>' + i + '</span> '
		  			}
		  		}
		  		pagination += ' ... <span>' + pages + '</span> '
		  	}
		  	
			$("#pages").html(pagination)

			$("#pages span").click(function(){
				currentPage = $(this).text()
				formatTable(arrItems, sortby, sort, parseInt(currentPage))
			})
			
			$("#table .row div").click(function(){
				var selected = $(this).text()
				$("#table .row").removeClass("active")
				$(this).parent().addClass("active")
				$(".content").html(selected)
			})
		}

		function parseDate(input) {
		  var parts = input.match(/(\d+)/g)
		  return parts[0] + parts[1] + parts[2]
		}

		function sortByKey(array, sortby, sort) {
			return array.sort(function (a, b) {
				if (sortby === "date") {
					var x = parseDate(a[sortby]); var y = parseDate(b[sortby]);
				}else{
					var x = a[sortby]; var y = b[sortby];
				}
				if (sort === "desc") {
					return ((x > y) ? -1 : ((x < y) ? 1 : 0));
				}else{
					return ((x < y) ? -1 : ((x > y) ? 1 : 0));
				}
			});
		}


		</script>

	</div>
</body>
</html>